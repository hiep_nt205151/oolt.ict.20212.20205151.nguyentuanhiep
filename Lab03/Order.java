
public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	private DigitalVideoDisc itemOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	int qtyOrdered = 0;
	
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if(qtyOrdered == MAX_NUMBERS_ORDERED) {
			System.out.println("The order is full"
					+ "There is "+ qtyOrdered +" discs in the list!");
			return;	
		}else {
			itemOrdered[qtyOrdered] = disc;
			qtyOrdered++;
			System.out.println("The disc has been added."
					+ "There is "+ qtyOrdered +" discs in the list!");
		}
	}
	
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		if(qtyOrdered == 0) {
			System.out.println("There is no disc!");
			return;
		}else {
			for(int i = 0; i<qtyOrdered; i++) {
				if(itemOrdered[i] == disc) {
					for(int j = i; j<qtyOrdered-1; j++) {
						itemOrdered[j] = itemOrdered[j+1];
					}
					qtyOrdered--;
					System.out.println("Remove disc successfully."
							+ "There is "+ qtyOrdered +" discs in the list!");
					return;
				}
			}
		}
	}
	
	public float totalCost() {
		float cost = 0;
		for (int i = 0; i<qtyOrdered; i++) {
			cost += itemOrdered[i].getCost();
		}
		return cost;
	}
	
	public void print() {
		System.out.printf("\n%-20s%-20s%-10s%-20s%s\n","Title","Category","Cost","Director","Length");
		for(int i=0; i<qtyOrdered;i++) {
			System.out.printf("%-20s%-20s%-10.2f%-20s%d\n",
					itemOrdered[i].getTitle(), itemOrdered[i].getCategory(),
					itemOrdered[i].getCost(), itemOrdered[i].getDirector(),
					itemOrdered[i].getLength());
		}
	}
}

package hust.soict.globalict.aims.media;

public class Disc extends Media{
	private int length;
	private String director;
	public int getLength() {
		return length;
	}
	public String getDirector() {
		return director;
	}
	public Disc(String title, String category, float cost) {
		super(title, category, cost);
		// TODO Auto-generated constructor stub
	}
	public Disc(String title, String id, String category, float cost) {
		super(title, id, category, cost);
		// TODO Auto-generated constructor stub
	}
	public Disc(String title, String category) {
		super(title, category);
		// TODO Auto-generated constructor stub
	}
	public Disc(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}
	public Disc() {
		super();
		// TODO Auto-generated constructor stub
	}
	

}

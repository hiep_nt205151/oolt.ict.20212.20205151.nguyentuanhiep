package hust.soict.globalict.aims;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.CompactDisc;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Track;
import hust.soict.globalict.aims.order.Order;
import hust.soict.globalict.aims.utils.MemoryDaemon;

public class Aims {
	public static Scanner sc = new Scanner(System.in);

	public static void showMenu() {
		System.out.println();
		System.out.println("Order management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");
		System.out.println();
	}

	public static Order createNewOrder() {
		return new Order();
	}

	public static void addDiscToOrder(Order order) {
		DigitalVideoDisc disc;
		String title;
		String id;
		String category;
		String director;
		int length;
		float cost;
		System.out.println("Enter title: ");
		title = sc.next();
		System.out.println("Enter id: ");
		id = sc.next();
		System.out.println("Enter category: ");
		category = sc.next();
		System.out.println("Enter director: ");
		director = sc.next();
		System.out.println("Enter length: ");
		length = sc.nextInt();
		System.out.println("Enter cost: ");
		cost = sc.nextFloat();
		disc = new DigitalVideoDisc(title, id, category, director, length, cost);
		order.addMedia(disc);
		System.out.println("Add disc to order successfuly");
		if (askForPlaying()) {
			disc.play();
		}

	}

	public static void addBookToOrder(Order order) {
		Book book = new Book();

		List<String> authors = new ArrayList<String>();
		System.out.println("Enter title: ");
		String title = sc.next();
		System.out.println("Enter id: ");
		String id = sc.next();
		System.out.println("Enter category: ");
		String category = sc.next();
		System.out.println("Enter cost: ");
		float cost = sc.nextFloat();
		System.out.println("Enter number of authors: ");
		int num = sc.nextInt();
		for (int i = 1; i <= num; i++) {
			System.out.println("Enter name of author " + i + ": ");
			authors.add(sc.next()); // input author name to the authors array list
		}
		book = new Book(title, id, category, cost); // create constructor
		book.setAuthors(authors);
		order.addMedia(book); // create a book

		System.out.println("Add book to order successfuly");
	}

	public static void addCompactDiscToOrder(Order order) {
		CompactDisc cpdisc = new CompactDisc();

		System.out.println("Enter title: ");
		String title = sc.next();
		System.out.println("Enter id: ");
		String id = sc.next();
		System.out.println("Enter category: ");
		String category = sc.next();
		System.out.println("Enter artist: ");
		String artist = sc.next();
		System.out.println("Enter cost: ");
		float cost = sc.nextFloat();
		cpdisc = new CompactDisc(title, id, category, cost, artist); // create constructor
		System.out.println("Enter the number of tracks: ");
		int numberOfTracks = sc.nextInt();

		for (int i = 1; i <= numberOfTracks; i++) {
			sc.nextLine();
			System.out.println("Enter the title of track " + i + ": ");
			String titleOfTrack = sc.nextLine();

			System.out.println("Enter the length of track " + i + ": ");
			int lengthOfTrack = sc.nextInt();

			Track track = new Track(titleOfTrack, lengthOfTrack);
			cpdisc.addTrack(track);
		}
		order.addMedia(cpdisc); // create a compact disc
		System.out.println("Add compact disc to order successfuly");
		if (askForPlaying()) {
			cpdisc.play();
		}

	}

	public static boolean askForPlaying() {
		System.out.println("Do you want to play the disc? (yes/no)");
		String choice = sc.nextLine();
		return choice.equals("yes");
	}

	public static void deleteItemById(Order order) {
		System.out.println("Enter id: ");
		String id = sc.next();
		order.removeMedia(id);
	}

	public static void main(String[] args) {
		int choice = -1;
		Order order = null;
		Scanner sc = new Scanner(System.in);
		while (choice != 0) {
			showMenu();
			choice = sc.nextInt();

			switch (choice) {
			case 1:
				order = createNewOrder();
				break;
			case 2:
				if (order != null) {
					System.out.println("1. Add Book");
					System.out.println("2. Add Digital video disc");
					System.out.println("3. Compact Disc");
					System.out.println("Enter choice: ");
					int ch = sc.nextInt();
					if (ch == 1)
						addBookToOrder(order);
					else if (ch == 2)
						addDiscToOrder(order);
					else if (ch == 3)
						addCompactDiscToOrder(order);
				} else {
					System.out.println("Please create an order first.");
				}
				break;
			case 3:
				if (order != null) {
					deleteItemById(order);
				} else {
					System.out.println("Please create an order first");
				}
				break;
			case 4:
				if (order != null) {
					order.printOrder();
				} else {
					System.out.println("Please create an order first");
				}
				break;
			case 0:
				break;
			}
		}
		sc.close();
		MemoryDaemon mem = new MemoryDaemon();
		Thread thread = new Thread(mem);
		thread.setDaemon(false);
		thread.start();
	}
}

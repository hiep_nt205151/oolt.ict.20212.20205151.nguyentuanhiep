package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.List;

public class Book extends Media{
	
	private List<String> authors = new ArrayList<String>();
	
	public Book() {
		
	}
	
	public Book(String title) {
		super(title);
	}
	
	public Book(String title, String category) {
		super(title,category);
	}
	
	public Book(String title, String category, float cost) {
		super(title,category,cost);
	}
	
	public Book(String title, String id, String category, float cost) {
		super(title,id,category,cost);
	}
	

	public Book(String title, String id, String category, float cost, List<String> authors) {
		super(title,id,category,cost);
		this.authors = authors;
	}

	public List<String> getAuthors() {
		return authors;
	}
	
	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
	
	public void addAuthor(String authorName) {
		if(authors.contains(authorName)) {
			System.out.println("This author is already in the list!");
			return;
		}
		authors.add(authorName);
		System.out.println("This author has been added to the list successfully");
	}
	
	public void removeAuthor(String authorName) {
		if(authors.contains(authorName)) {
			authors.remove(authorName);
			System.out.println("This author has been removed from the list successfully");
		}
		System.out.println("This author is not in the list!");
		return;
	}

}

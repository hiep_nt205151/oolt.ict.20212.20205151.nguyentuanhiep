package hust.soict.globalict.aims.order;

import java.util.ArrayList;

import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.CompactDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.utils.MyDate;
import hust.soict.globalict.aims.media.DigitalVideoDisc;

public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	public static final int MAX_LIMITTED_ORDERS = 4;
	private static int nbOrders = 0;
	private MyDate dateOrdered;

	public Order() {
		MyDate date = new MyDate();
		this.dateOrdered = date;
		if (nbOrders < MAX_LIMITTED_ORDERS)
			nbOrders++;
		else
			System.out.println("You can not add any order due to limitation reached");
	}

	private ArrayList<Media> itemOrdered = new ArrayList<Media>();

	public void addMedia(Media media) {
		if (itemOrdered.size() == MAX_NUMBERS_ORDERED) {
			System.out.println("The order is full" + "There is " + itemOrdered.size() + " media in the order list!");
			return;
		} else {
			itemOrdered.add(media);
			System.out.println("The media has been added." + "There is " + itemOrdered.size() + " media in the order list!");
			return;
		}
	}

	public void removeMedia(Media media) {
		if (itemOrdered.size() == 0)
			System.out.println("List is empty");
		else if (itemOrdered.contains(media) == false) {
			System.out.println("There is no " + media + " in the list");
			return;
		} else {
			itemOrdered.remove(media);
			System.out
			        .println("Remove media successfully. " + "There is " + itemOrdered.size() + " media in the list.");
			return;
		}
	}

	public void removeMedia(String id) {
		for (int i = 0; i < itemOrdered.size(); i++) {
			if (itemOrdered.get(i).getId().equals(id)) {
				itemOrdered.remove(i);
				System.out.println("Remove media successfully. " 
				+ "There is " + itemOrdered.size() + " media in the list.");
				return;
			}
		}
		System.out.println("There is no media with this ID in the list");
		return;
	}

	public float totalCost() {
		float cost = 0;
		for (int i = 0; i < itemOrdered.size(); i++) {
			cost += itemOrdered.get(i).getCost();
		}
		return cost;
	}

	public void printDisc(int i) {
		System.out.println((i + 1) + ". DVD - [" + itemOrdered.get(i).getTitle() + "] - ["
		        + itemOrdered.get(i).getCategory() + "] - ["
		        + ((DigitalVideoDisc)itemOrdered.get(i)).getDirector() + "] - ["
		        + ((DigitalVideoDisc)itemOrdered.get(i)).getLength() + "]: [" 
		        + itemOrdered.get(i).getCost()
		        + "] $");
	}
	
	public void printBook(int i) {
		System.out.println((i + 1) + ". Book - [" + itemOrdered.get(i).getTitle() + "] - ["
		        + itemOrdered.get(i).getCategory() + "] - " 
				+ ((Book) itemOrdered.get(i)).getAuthors() + " : [" 
		        + itemOrdered.get(i).getCost() + "] $");
	}
	
	public void printCompactDisc(int i) {
		System.out.println((i + 1) + ". Compact Disc - [" + itemOrdered.get(i).getTitle() + "] - ["
		        + itemOrdered.get(i).getCategory() + "] - [" 
				+ itemOrdered.get(i).getId() + " : [" 
		        + itemOrdered.get(i).getCost() + "] $");
	}
	
	public void printOrder() {
		System.out.println();
		System.out.println("*******************************Order***********************************");
		dateOrdered.print();
		System.out.println("Ordered Items: ");
		if(itemOrdered.size() == 0) {
			System.out.println("There is no media in the order list");
			return;
		}
		for (int i = 0; i < itemOrdered.size(); i++) {
			if (itemOrdered.get(i) instanceof DigitalVideoDisc)
				printDisc(i);
			else if (itemOrdered.get(i) instanceof Book)
				printBook(i);
			else if (itemOrdered.get(i) instanceof CompactDisc)
				printCompactDisc(i);
		}

		System.out.println("Total Cost: " + totalCost());
		System.out.println("**********************************************************************");
		System.out.println();
	}

	public Media getALuckyItem() {
		int max = itemOrdered.size();
		int min = 1;

		int number = (int) Math.floor(Math.random() * (max - min + 1) + 1);
//		while (itemOrdered.get(number) instanceof DigitalVideoDisc == false) {
//			number = (int) Math.floor(Math.random() * (max - min + 1) + 1);
//		}
		Media media = (Media) itemOrdered.get(number-1); //index starts from 0
		media.setCost(0.0f);
		System.out.println("Congratualation! You are lucky have 1 disc for free: " + media.getTitle());
		printOrder();
		return media;
	}
}

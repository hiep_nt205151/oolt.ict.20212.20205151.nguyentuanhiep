import java.util.Scanner;

public class DayOfMonth {
	public static void main(String[] strings) {
        Scanner input = new Scanner(System.in);
        int numberOfDays = 0; 
        System.out.print("Input month: ");
        String month = input.nextLine();

        System.out.print("Input year: ");
        int year = input.nextInt();

        switch (month) {
            case "1": case "January": case "Jan": case "Jan.":
                numberOfDays = 31;
                break;
            case "2": case "February": case "Feb": case "Feb.":
                if (checkLeapYear(year)==true)
                    numberOfDays = 29;
                else
                    numberOfDays = 28;
                break;
            case "3": case "March": case "Mar": case "Mar.":
                numberOfDays = 31;
                break;
            case "4": case "April": case "Apr": case "Apr.":
                numberOfDays = 30;
                break;
            case "5": case "May":
                numberOfDays = 31;
                break;
            case "6": case "June": case "Jun":
                numberOfDays = 30;
                break;
            case "7": case "July": case "Jul":
                numberOfDays = 31;
                break;
            case "8": case "August": case "Aug": case "Aug.":
                numberOfDays = 31;
                break;
            case "9": case "September": case "Sep": case "Sep.":
                numberOfDays = 30;
                break;
            case "10": case "October": case "Oct": case "Oct.":
                numberOfDays = 31;
                break;
            case "11": case "November": case "Nov": case "Nov.":
                numberOfDays = 30;
                break;
            case "12": case "December": case "Dec": case "Dec.":
                numberOfDays = 31;
                break;
        }
        input.close();
        System.out.println("This month has " + numberOfDays + " days");
    }
	
	public static boolean checkLeapYear(int year) {
		boolean leap = false;
		if(year%4==0) {
			if(year%100!=0)
				leap = true;
			else {
				if(year%400==0)
					leap = true;
			}
		}
		return leap;
	}
}

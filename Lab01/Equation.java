import java.util.Scanner;
import java.lang.Math;
public class Equation {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Program to solve: ");
        System.out.println("1. The first-degree equation (linear equation) with one variable");
        System.out.println("2. The system of first-degree equations (linear system) with two variables");
        System.out.println("3. The second-degree equation with one variable");
        System.out.print("Enter choice: ");
        int n = sc.nextInt();
        switch (n){
            case 1:
                linearEquation();
                break;
            case 2:
                linearSystem();
                break;
            case 3:
                quadraticEquation();
                break;
            default:
        }
    }
    static void linearEquation(){
        Scanner sc = new Scanner(System.in);
        System.out.println("1. The first-degree equation (linear equation) with one variable:");
        System.out.println("ax + b = 0");
        System.out.print("Enter a: ");
        double a = sc.nextDouble();
        while(a == 0){
            System.out.println("a cannot equal to 0. Try again!");
            System.out.print("Enter a: ");
            a = sc.nextDouble();
        }
        System.out.print("Enter b: ");
        double b = sc.nextDouble();
        double x = -b/a;
        System.out.print("the equation has a unique solution x =  " + x);
    }
    
    static void linearSystem(){
        Scanner sc = new Scanner(System.in);
        System.out.println("2. The system of first-degree equations (linear system) with two variables:");
        System.out.println("a_11*x_1 + a_12*x_2 = b_1\na_21*x_1 + a_22*x_2 = b_2");
        System.out.println("Enter coefficients of each variable for 1st equations");
        double a_11 = sc.nextDouble();
        double a_12 = sc.nextDouble();
        double b_1 = sc.nextDouble();
        System.out.println("Enter coefficients of each variable for 2nd equations");
        double a_21 = sc.nextDouble();
        double a_22 = sc.nextDouble();
        double b_2 = sc.nextDouble();
        double det = a_11*a_22 - a_21*a_12;
        double det1 = b_1*a_22 - b_2 * a_12;
        double det2 = a_11*b_2 - a_21 * b_1;
        if(det == 0){
            if(det1 != 0 || det2 !=0)
                System.out.println("The system has no solution");
            if(det1 == 0 && det2 == 0)
                System.out.println("the system has infinitely many solutions");
        }else
        System.out.println("The system has a unique solution (x_1, x_2) = (" + det1/det + "," + det2/det + ")");
    }

    static void quadraticEquation(){
        Scanner sc = new Scanner(System.in);
        System.out.println("3. The second-degree equation with one variable");
        System.out.println("a*x^2 + b*x = c");
        System.out.println("Enter cofficients of the equation: ");
        double a = sc.nextDouble();
        double b = sc.nextDouble();
        double c = sc.nextDouble();
        double delta = b*b - 4*a*c;
        if(delta < 0)
            System.out.println("The equation has no solution!");
        else if(delta == 0){
            double x = -b/(2*a);
            System.out.println("The equation has double root x_1 = x_2 = " + x);
        }else{
            double x_1 = (-b - Math.sqrt(delta))/(2*a);
            double x_2 = (-b + Math.sqrt(delta))/(2*a);
            System.out.println("The equation has 2 distinct roots x_1 = " + x_1 + " and x_2 = " + x_2);
        }
    }
}


package hust.soict.globalict.aims.media;

public class DigitalVideoDisc extends Disc implements Playable{
	private String director;
	private int length;

	public DigitalVideoDisc() {

	}

	public DigitalVideoDisc(String title) {
		super(title);
	}

	public DigitalVideoDisc(String title, String category) {
		super(title, category);
	}

	public DigitalVideoDisc(String title, String category, String director) {
		super(title, category);
		this.director = director;
	}

	public DigitalVideoDisc(String title, String id, String category, String director, int length, float cost) {
		super(title, id, category, cost);
		this.director = director;
		this.length = length;
	}

	public DigitalVideoDisc(String title, String id, String category, float cost) {
		super(title, id, category, cost);

	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}

	public boolean search(String title) {
		String[] titleSplit = title.split(" ");
		for (int i = 0; i < titleSplit.length; i++) {
			if (this.title.toLowerCase().contains(titleSplit[i].toLowerCase()) == false)
				return false;
		}
		return true;
	}

	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("Playing DVD: " + this.getTitle());
        System.out.println("DVD length: " + this.getLength());
	}
}

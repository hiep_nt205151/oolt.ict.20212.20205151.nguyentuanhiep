import java.util.Scanner;

public class Sort {
	public static void quickSort(int arr[], int start, int end) {
		if(start<end) {
			int pIndex = partition(arr,start,end);
			quickSort(arr,start,pIndex-1);
			quickSort(arr,pIndex+1,end);
		}
	}
	
	public static int partition(int arr[], int start, int end) {
		int pivot = arr[end];
		int pIndex = start;
		int tem;
		for(int i = start; i<end;i++) {
			if(arr[i]<=pivot) {
				tem = arr[i];
				arr[i] = arr[pIndex];
				arr[pIndex] = tem;
				pIndex++;
			}
		}
		tem = arr[pIndex];
		arr[pIndex] = arr[end];
		arr[end] = tem;
		return pIndex;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number of elements in array: ");
		int n = sc.nextInt();
		
		int[] arr = new int[n];
		for(int i = 0;i<n;i++) {
			System.out.printf("Enter element of order %d in the array: ",i);
			arr[i] = sc.nextInt();
		}
		
		System.out.printf("%-20s","Before sorting: ");
		for(int i = 0; i<n;i++) {
			System.out.printf("%4d",arr[i]);
		}
		
		quickSort(arr,0,n-1);
		
		System.out.printf("\n%-20s","After sorting: ");
		for(int i = 0; i<n;i++) {
			System.out.printf("%4d",arr[i]);
		}
		sc.close();
	}
}

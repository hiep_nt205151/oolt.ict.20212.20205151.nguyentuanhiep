package hust.soict.globalict.aims;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.order.Order;

public class Aims {
	public static Scanner sc = new Scanner(System.in);
	public static void showMenu() {
     	System.out.println("Order management Application: ");
     	System.out.println("--------------------------------");
     	System.out.println("1. Create new order");
     	System.out.println("2. Add item to the order");
     	System.out.println("3. Delete item by id");
     	System.out.println("4. Display the items list of order");
     	System.out.println("0. Exit");
     	System.out.println("--------------------------------");
     	System.out.println("Please choose a number: 0-1-2-3-4");
     }
	
	public static Order createNewOrder() {
		return new Order();
	}
	
	public static void addDiscToOrder(Order order) {
        String title;
        String id;
        String category;
        String director;
        int length;
        float cost;
        System.out.println("Enter title: ");
        title = sc.next();
        System.out.println("Enter id: ");
        id = sc.next();
        System.out.println("Enter category: ");
        category = sc.next();
        System.out.println("Enter director: ");
        director = sc.next();
        System.out.println("Enter length: ");
        length = sc.nextInt();
        System.out.println("Enter cost: ");
        cost = sc.nextFloat();
        order.addMedia(new DigitalVideoDisc(title, id, category,director,length, cost));
        System.out.println("Add disc to order successfuly");
    }
	
	public static void addBookToOrder(Order order) {
		Book book1 = new Book();
        String title;
        String id;
        String category;
        float cost;
        
        List<String> authors = new ArrayList<String>();
        System.out.println("Enter title: ");
        title = sc.next();
        System.out.println("Enter id: ");
        id = sc.next();
        System.out.println("Enter category: ");
        category = sc.next();
        System.out.println("Enter cost: ");
        cost = sc.nextFloat();
        System.out.println("Enter number of authors: ");
        int num = sc.nextInt();
        for(int i = 0; i<num; i++) {
        	authors.add(sc.next());
        }
        for(String i:authors) {
        	book1.addAuthor(i);
        
        }
        order.addMedia(new Book(title, id, category, cost,authors));
        System.out.println("Add book to order successfuly");
    }
	
	public static void deleteItemById(Order order) {
		System.out.println("Enter id: ");
		String id = sc.next();
		order.removeMedia(id);
	}
	
	public static void main(String[] args) {
        int choice = -1;
        Order order = null;
        Scanner sc = new Scanner(System.in);
        while(choice != 0) {
            showMenu();
            choice = sc.nextInt();

            switch (choice) {
                case 1:
                    order = createNewOrder();
                    break;
                case 2:
                    if (order != null) {
                    	System.out.println("1. Add Book");
                    	System.out.println("2. Add Digital video disc");
                    	System.out.println("Enter choice: ");
                    	int ch = sc.nextInt();
                    	if(ch == 1)
                    		addBookToOrder(order);
                    	else if(ch == 2)
                    		addDiscToOrder(order);
                    } else {
                        System.out.println("Please create an order first.");
                    }
                    break;
                case 3:
                    if (order != null) {
                        deleteItemById(order);
                    } else {
                        System.out.println("Please create an order first");
                    }
                    break;
                case 4:
                    if (order != null) {
                        order.printOrder();
                    } else {
                        System.out.println("Please create an order first");
                    }
                    break;
                case 0:
                    break;
            }
        }
        sc.close();
    
    }	
}

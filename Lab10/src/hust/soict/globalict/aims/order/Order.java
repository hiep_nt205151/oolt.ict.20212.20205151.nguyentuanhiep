package hust.soict.globalict.aims.order;

import java.util.ArrayList;
import java.util.List;

import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.utils.MyDate;

public class Order {
	public int id;
	public static final int MAX_NUMBERS_ORDERED =10 ;
	private List<Media> itemsOrdered = new ArrayList<Media>();
	
	private MyDate dateOrdered;
	public static final int MAX_LIMITEDS_ORDERS =5 ;
	private static int nbOrders=0;
	public static int luckyNumber=-1;
	public Order() {
		MyDate date= new MyDate();
		this.dateOrdered=date;
		if(nbOrders<MAX_LIMITEDS_ORDERS)
			nbOrders++;
		else 
			System.out.println("You can not add any order due to limitation reached");
		
	}
	public void addMedia(Media media){
		if (itemsOrdered.size() == MAX_NUMBERS_ORDERED){
			System.out.println("Your order is already full.");
			return;
		}
		else{
			int check =0;
			for(int i=0;i<itemsOrdered.size();++i) {
				if (media.equals(itemsOrdered)) check++;
			}
			if(check==0) {
				media.setId(itemsOrdered.size() + 1);
				itemsOrdered.add(media);
				System.out.println("Order has been updated. Your order now has "+ itemsOrdered.size() + " items");
			}else System.out.println("This media is already ordered");
		}
	}

	public void removeMedia(int id){
		if (itemsOrdered.size() == 0){
			System.out.println("Your order has no items to be removed.");
			return;
		}
		int search = -1;
		for (int i = 0; i < itemsOrdered.size(); i++) {
            if (itemsOrdered.get(i).getId() == id)
                search = i;
        }
        if (search == -1)
            System.out.println("The media is not in the list.");
        else{
            itemsOrdered.remove(search);
			for (int i = search; i < itemsOrdered.size(); i++) {
				itemsOrdered.get(i).setId(i+1);
			}
            System.out.println("Media removed successfully!");
        }
	}

	public void removeMedia(Media media){
		if (itemsOrdered.size() == 0){
			System.out.println("Your order has no items to be removed.");
			return;
		}
		int search = -1;
		for (int i = 0; i < itemsOrdered.size(); i++) {
            if (itemsOrdered.get(i).getTitle().equals(media.getTitle()))
                search = i;
        }
        if (search == -1)
            System.out.println("The media is not in the list.");
        else{
            itemsOrdered.remove(search);
			for (int i = search; i < itemsOrdered.size(); i++) {
				itemsOrdered.get(i).setId(i+1);
			}
            System.out.println("Media removed successfully!");
        }
	}
	
	public float totalCost() {
		float sum = 0;
		for (int i = 0; i < itemsOrdered.size(); i++) {
			if (itemsOrdered.get(i).equals(itemsOrdered.get(getALuckyItem())))
				System.out.println("Your free item: " + itemsOrdered.get(i).getTitle());
			else
				sum += itemsOrdered.get(i).getCost();
		}
		return sum;
	}


	public void printOrder() {
		System.out.println("*************************Order*************************************************************");
		System.out.println("Date[" + dateOrdered.printDate() + "]");
		System.out.println("Ordered Items:");
		for (int i = 0; i < itemsOrdered.size(); ++i) {
			System.out.println(itemsOrdered.get(i).getId() + ". Item -[" + itemsOrdered.get(i).getTitle() + "] - [" + itemsOrdered.get(i).getCategory() + "]-[" + itemsOrdered.get(i).getCost() + "] $");
		}
		System.out.println("Total cost: " + this.totalCost());
		System.out.println("*******************************************************************************************");
	}
	public MyDate getDateOrdered() {
		return dateOrdered;
	}
	public void setDateOrdered(MyDate dateOrdered) {
		this.dateOrdered = dateOrdered;
	}
	public int getALuckyItem(){
		return (int) (Math.random() * itemsOrdered.size());
	}
}

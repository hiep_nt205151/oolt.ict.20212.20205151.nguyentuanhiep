package hust.soict.globalict.aims;
import hust.soict.globalict.aims.disc.DigitalVideoDisc;
import hust.soict.globalict.aims.order.Order;

public class DiskTest {

	public static void main(String[] args) {
		DigitalVideoDisc disc = new DigitalVideoDisc("Harry Potter");
		System.out.println(disc.search("POTTER    HARRY"));
		
		Order order = new Order();
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("disc 1");
        dvd1.setCategory("Animation");
        dvd1.setCost(19.95f);
        dvd1.setDirector("Roger Allers");
        dvd1.setLength(87);
        order.addDigitalVideoDisc(dvd1);
        
        DigitalVideoDisc dvd2 = new DigitalVideoDisc("disc 2");
        dvd2.setCategory("Science Fiction");
        dvd2.setCost(24.95f);
        dvd2.setDirector("George Lucas");
        dvd2.setLength(124);
        order.addDigitalVideoDisc(dvd2);

        order.getALuckyItem();
	}

}

package hust.soict.globalict.aims;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.order.Order;

public class DiskTest {

	public static void main(String[] args) {
		DigitalVideoDisc disc = new DigitalVideoDisc("Harry Potter");
		System.out.println(disc.search("POTTER    HARRY"));
		
		Order order = new Order();
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("disc 1");
        dvd1.setCategory("Animation");
        dvd1.setCost(19.95f);
        dvd1.setDirector("Roger Allers");
        dvd1.setLength(87);
        order.addMedia(dvd1);
        
        DigitalVideoDisc dvd2 = new DigitalVideoDisc("disc 2");
        dvd2.setCategory("Science Fiction");
        dvd2.setCost(24.95f);
        dvd2.setDirector("George Lucas");
        dvd2.setLength(124);
        order.addMedia(dvd2);
        
        DigitalVideoDisc dvd3 = new DigitalVideoDisc("disc 3");
        dvd3.setCategory("Science Fiction");
        dvd3.setCost(24.95f);
        dvd3.setDirector("George Lucas");
        dvd3.setLength(124);
        order.addMedia(dvd3);
        
        DigitalVideoDisc dvd4 = new DigitalVideoDisc("disc 4");
        dvd4.setCategory("Science Fiction");
        dvd4.setCost(24.95f);
        dvd4.setDirector("George Lucas");
        dvd4.setLength(124);
        order.addMedia(dvd4);

        order.getALuckyItem();
	}

}

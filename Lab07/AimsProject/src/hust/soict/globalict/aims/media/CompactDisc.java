package hust.soict.globalict.aims.media;

import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable{
	private String artist;

	public CompactDisc() {
		super();
		// TODO Auto-generated constructor stub
	}


	public CompactDisc(String title, String category, float cost) {
		super(title, category, cost);
		// TODO Auto-generated constructor stub
	}


	public CompactDisc(String title, String id, String category, float cost) {
		super(title, id, category, cost);
		// TODO Auto-generated constructor stub
	}


	public CompactDisc(String title, String id, String category, float cost, String artist) {
		super(title, id, category, cost);
		this.artist = artist;
	}


	public CompactDisc(String title, String category) {
		super(title, category);
		// TODO Auto-generated constructor stub
	}


	public CompactDisc(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}


	private ArrayList<Track> tracks = new ArrayList<Track>();
	
	
	public String getArtist() {
		return artist;
	}


	public void addTrack(Track track) {
		if(tracks.contains(track)) {
			System.out.println("This track is already in the list");
			return;
		}
		tracks.add(track);
		System.out.println("This track has been added to the list successfully!");
		return;
	}
	


	public int getLength() {
		int totalLength = 0;
		for(Track track : tracks) {
			totalLength += track.getLength();
		}
		return totalLength;
	}


	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("Information of compact disc: ");
		System.out.println("Title: " + this.getTitle() + 
				". Artist: " + this.getArtist());
//		System.out.println(this.getTitle() + " - " + this.getArtist());
		for(Track track : tracks) {
			track.play();
		}
	}
}

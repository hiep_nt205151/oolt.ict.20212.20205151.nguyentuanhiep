
public class DateTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MyDate date1 = new MyDate();
		date1.print();
		MyDate date2 = new MyDate(1,9,2002);
		date2.print();
		MyDate date3 = new MyDate("September 11 2002");
		date3.print();
		date3.accept();
		date3.print();
		MyDate[] date= new MyDate[3];
		date[0]=date1;
		date[1]=date2;
		date[2]=date3;
		DateUtils.dateCompare(date1, date2);
		DateUtils.sort(date);
	}
}

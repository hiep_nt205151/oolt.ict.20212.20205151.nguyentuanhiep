package hust.soict.globalict.aims;
import hust.soict.globalict.aims.disc.DigitalVideoDisc;
import hust.soict.globalict.aims.order.Order;

public class Aims {
    public static void main(String[] args) {
        Order anOrder = new Order();
        Order Order2 = new Order();
        Order Order3 = new Order();
        Order Order4 = new Order();
        
        DigitalVideoDisc dvd1 = new DigitalVideoDisc("disc 1");
        dvd1.setCategory("Animation");
        dvd1.setCost(19.95f);
        dvd1.setDirector("Roger Allers");
        dvd1.setLength(87);
        Order2.addDigitalVideoDisc(dvd1);
        
        DigitalVideoDisc dvd2 = new DigitalVideoDisc("disc 2");
        dvd2.setCategory("Science Fiction");
        dvd2.setCost(24.95f);
        dvd2.setDirector("George Lucas");
        dvd2.setLength(124);
        Order3.addDigitalVideoDisc(dvd2);

        DigitalVideoDisc dvd3 = new DigitalVideoDisc("disc 3");
        dvd3.setCategory("Animation");
        dvd3.setCost(18.99f);
        dvd3.setDirector("John Musker");
        dvd3.setLength(90);
        Order4.addDigitalVideoDisc(dvd3);

        DigitalVideoDisc[] dvdList = new DigitalVideoDisc[3];
        dvdList[0] = dvd1;
        dvdList[1] = dvd2;
        dvdList[2] = dvd3;
        anOrder.addDigitalVideoDisc(dvdList);
        System.out.printf("Total Cost is %.2f$\n",anOrder.totalCost());
//        anOrder.removeDigitalVideoDisc(dvd1);
        System.out.printf("Total Cost is %.2f$\n",anOrder.totalCost());
        
        DigitalVideoDisc dvd4 = new DigitalVideoDisc("disc 4");
        dvd4.setCategory("Science Fiction");
        dvd4.setCost(24.95f);
        dvd4.setDirector("George Lucas");
        dvd4.setLength(124);
        anOrder.addDigitalVideoDisc(dvd4);
        
        DigitalVideoDisc dvd5 = new DigitalVideoDisc("disc 5");
        dvd5.setCategory("Science Fiction");
        dvd5.setCost(24.95f);
        dvd5.setDirector("George Lucas");
        dvd5.setLength(124);
        anOrder.addDigitalVideoDisc(dvd5);
        
        DigitalVideoDisc dvd6 = new DigitalVideoDisc("disc 6");
        dvd6.setCategory("Science Fiction");
        dvd6.setCost(24.95f);
        dvd6.setDirector("George Lucas");
        dvd6.setLength(124);
        anOrder.addDigitalVideoDisc(dvd6);
        
        DigitalVideoDisc dvd7 = new DigitalVideoDisc("disc 7");
        dvd7.setCategory("Science Fiction");
        dvd7.setCost(24.95f);
        dvd7.setDirector("George Lucas");
        dvd7.setLength(124);
        anOrder.addDigitalVideoDisc(dvd7);
        anOrder.printOrder();
        Order2.printOrder();
    }	
}

import java.util.Scanner;  // Import the Scanner class

public class ComputeTwoNumbers {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);    //Create a Scanner object
        System.out.print("Enter the first number: ");
        String strNum1 = sc.nextLine();         //input String strNum1
        double num1 = Double.parseDouble(strNum1); //String to Double
        //double num1 = sc.nextDouble();        //input Double num1
        System.out.print("Enter the second number: ");  
        double num2 = sc.nextDouble();          //input Double num2
        System.out.println("Sum = " + sum(num1, num2) +
                            "\nProduct = " + product(num1, num2) +
                            "\nDifference = " + difference(num1, num2) +
                            "\nQuotient = " + quotient(num1, num2));
    }
    public static Double sum(double num1, double num2){
        return num1 + num2;
    }
    public static Double difference(double num1, double num2){
        return num1 - num2;
    }
    public static Double product(double num1, double num2){
        return num1 * num2;
    }
    public static Double quotient(double num1, double num2){
        if (num2 == 0) 
            System.out.println("The disivor cannot be 0");
        return num1/num2;
    }
}
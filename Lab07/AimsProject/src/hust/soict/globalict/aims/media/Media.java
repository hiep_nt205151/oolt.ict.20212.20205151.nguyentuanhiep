package hust.soict.globalict.aims.media;

public abstract class Media {
	public String title;
	public String id;
	public String category;
	public float cost;
	
	public Media() {
		
	}
	
	public Media(String title) {
		super();
		this.title = title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}

	public Media(String title, String category) {
		super();
		this.title = title;
		this.category = category;
	}

	public Media(String title, String category, float cost) {
		super();
		this.title = title;
		this.category = category;
		this.cost = cost;
	}
	
	public Media(String title, String id, String category, float cost) {
		super();
		this.title = title;
		this.id = id;
		this.category = category;
		this.cost = cost;
	}

	public String getTitle() {
		return title;
	}
//	public void setTitle(String title) {
//		this.title = title;
//	}
	public String getCategory() {
		return category;
	}
//	public void setCategory(String category) {
//		this.category = category;
//	}
	public float getCost() {
		return cost;
	}
//	public void setCost(float cost) {
//		this.cost = cost;
//	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	
	

}

import java.util.Scanner;

public class AddMatrix {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number of rows and columns of 2 matrices: ");
		int row = sc.nextInt();
		int col = sc.nextInt();
		sc.close();
		int a[][] = new int[row][col];
		int b[][] = new int[row][col];
		int sum[][] = new int[row][col];
		
		System.out.println("Input entries of the first matrix a[][]");
		for(int i = 0; i<row; i++) {
			for(int j = 0;j<col;j++) {
				System.out.printf("Enter element a[%d][%d]: ",i,j);
				a[i][j] = sc.nextInt();
			}
		}
		
		System.out.println("Input entries of the second matrix ");
		for(int i = 0; i<row; i++) {
			for(int j = 0;j<col;j++) {
				System.out.printf("Enter element b[%d][%d]: ",i,j);
				b[i][j] = sc.nextInt();
			}
		}
		
		for(int i = 0; i<row; i++) {
			for(int j = 0;j<col;j++) {
				sum[i][j] = a[i][j] + b[i][j];
			}
		}
		
		System.out.println("The sum of 2 matrices: ");
		for(int i = 0; i<row; i++) {
			for(int j = 0;j<col;j++) {
				System.out.printf("%-4d",sum[i][j]);
			}
			System.out.println();
		}
	}

}
